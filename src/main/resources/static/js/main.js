
function Authorised() {
    if(localStorage.getItem("user") === null){
        document.getElementById("btn-register").hidden = false;
        document.getElementById("btn-login").hidden = false;
    }

    else{

        document.getElementById("btn-register").hidden = true;
        document.getElementById("btn-login").hidden = true;
        let nav = document.getElementsByClassName("navbar")[0];
        let div = document.createElement('div');

        const html = `
            <a class="navbar-brand ml-3" href="/main/profile">
                <button type="button" class="btn btn-primary " id="btn-login">Profile</button>
                </a>
                <a class="navbar-brand " href="/main/logout">
                <button type="button" class="btn btn-primary" onclick="localStorage.clear()" id="btn-register">Logout</button>
                </a>
                <a class="navbar-brand " href="/main/newTheme">
                <button type="button" class="btn btn-primary" id="btn-register">New theme</button>
                </a>
                `;

        div.innerHTML += html;
        nav.insertBefore(div, nav.childNodes[2]);

    }
}
