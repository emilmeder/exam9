use `forum`;

CREATE TABLE `customers` (
     `id` INT NOT NULL AUTO_INCREMENT,
     `email` VARCHAR(128) NOT NULL,
     `password` VARCHAR(128) NOT NULL,
     `fullname` VARCHAR(128) NOT NULL,
     `enabled` boolean NOT NULL,
     `role` VARCHAR(16) NOT NULL,
     PRIMARY KEY (`id`)
);

CREATE TABLE `posts` (
     `id` INT NOT NULL AUTO_INCREMENT,
     `name` VARCHAR(50) NOT NULL,
     `text` VARCHAR(300) NOT NULL,
     `date` DATETIME NOT NULL,
     `customers_id` INT NOT NULL,
     PRIMARY KEY (`id`),
     CONSTRAINT `fk_posts_customers` FOREIGN KEY (`customers_id`)REFERENCES `customers` (`id`)
);

CREATE TABLE `comments` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `text` VARCHAR(500) NULL,
    `date` DATETIME NULL,
    `posts_id` INT NOT NULL,
    `customers_id` INT NOT NULL,
    PRIMARY KEY (`id`, `posts_id`, `customers_id`),
    CONSTRAINT `fk_comments_posts1`
        FOREIGN KEY (`posts_id`)REFERENCES `posts` (`id`),
    CONSTRAINT `fk_comments_customers1`
        FOREIGN KEY (`customers_id`)REFERENCES `customers` (`id`)
);

insert into `customers` (email, password, fullname, enabled, role) VALUES
('Johny@mail.ru','123','Johny-3',true,'USER'),
('Johny@mail.ru','123','Johny-Scene',true,'USER'),
('Johny@mail.ru','123','Johny',true,'USER'),
('Johny@mail.ru','123','J-Dog',true,'USER');


insert into `posts` ( name, text, date, customers_id) VALUES
('SomeonesPost','SomeonesPostDescription','2020-01-01 00:30:30' ,1),
('SomeonesPost','SomeonesPostDescription','2020-01-01 00:32:30' ,3),
('SomeonesPost','SomeonesPostDescription','2020-01-01 00:36:30' ,2),
('SomeonesPost','SomeonesPostDescription','2020-01-01 00:35:30' ,1),
('SomeonesPost','SomeonesPostDescription','2020-01-01 00:44:30' ,4),
('SomeonesPost','SomeonesPostDescription','2020-01-01 00:40:30' ,2),
('SomeonesPost','SomeonesPostDescription','2020-01-01 00:45:30' ,4),
('SomeonesPost','SomeonesPostDescription','2020-01-01 00:40:30' ,2);

# insert into `posts` ( name, text, date, customers_id) VALUES
# ('SomeonesPost','SomeonesPost1','2020-00-00 00:00:00' ,1),
# ('SomeonesPost','SomeonesPost2','2020-00-00 00:00:00' ,2),
# ('SomeonesPost','SomeonesPost3','2020-00-00 00:00:00' ,1),
# ('SomeonesPost','SomeonesPost4','2020-00-00 00:00:00' ,2),
# ('SomeonesPost','SomeonesPost5','2020-00-00 00:00:00' ,1);
