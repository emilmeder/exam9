//package kz.attractor.exam9.Controller;
//
//import kz.attractor.exam9.model.Customer;
//import kz.attractor.exam9.model.CustomerRegisterForm;
//import kz.attractor.exam9.repository.CustomerRepository;
//import kz.attractor.exam9.service.CustomerService;
//import lombok.AccessLevel;
//import lombok.AllArgsConstructor;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.validation.BindingResult;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.servlet.mvc.support.RedirectAttributes;
//
//import javax.servlet.http.HttpSession;
//import java.util.UUID;
//
//@Controller
//@AllArgsConstructor(access = AccessLevel.PRIVATE)
//public class LoginController {
//
//    public final CustomerService cs;
//    public final CustomerRepository customerRepository;
//
//
//
//
//    @GetMapping("/register")
//    public String pageRegisterCustomer(Model model) {
//        if (!model.containsAttribute("dto")) {
//            model.addAttribute("dto", new CustomerRegisterForm());
//        }
//
//        return "register";
//    }
//
//    @PostMapping("/register")
//    public String registerPage(CustomerRegisterForm customerRequestDto,
//                               BindingResult validationResult,
//                               RedirectAttributes attributes) {
//        attributes.addFlashAttribute("dto", customerRequestDto);
//
//        if (validationResult.hasFieldErrors()) {
//            attributes.addFlashAttribute("errors", validationResult.getFieldErrors());
//            return "redirect:/login";
//        }
//
//        cs.register(customerRequestDto);
//        return "redirect:/login";
//    }
//
//    @GetMapping("/login")
//    public String loginPage(@RequestParam(required = false, defaultValue = "false") Boolean error, Model model) {
//        model.addAttribute("error", error);
//        return "login";
//    }
//
//    @GetMapping("/logout")
//    public String invalidate(HttpSession session) {
//        if (session != null) {
//
//            session.invalidate();
//        }
//
//        return "redirect:/index";
//    }
//
//
////    @GetMapping("/register")
////    public String register(){
////
////        return "reg";
////    }
////    @GetMapping("/login")
////    public String login(){
////
////        return "log";
////    }
////
////    @PostMapping("/register")
////    public String makeUser(
////        @Valid @RequestParam("mail") String mail,
////        @Valid @RequestParam("name") String name,
////        @Valid @RequestParam("login") String login,
////        @Valid @RequestParam("password") String password){
////
////        User user = new User();
////        user.setMail(mail);
////        user.setName(name);
////        user.setLogin(login);
////        user.setPassword(password);
////        ur.save(user);
////        return "redirect:/login";
////    }
//}