package kz.attractor.exam9.Controller;

import kz.attractor.exam9.model.CustomerRegisterForm;
import kz.attractor.exam9.repository.CustomerRepository;
import kz.attractor.exam9.service.CommentService;
import kz.attractor.exam9.service.CustomerService;
import kz.attractor.exam9.service.PostService;
import kz.attractor.exam9.service.PropertiesService;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@RequestMapping("/main")
public class MainController {
    private final CustomerService cs;
    private final PostService ps;
    private final PropertiesService propertiesService;
    private final CommentService commentService;

    @GetMapping
    public String indexPage(Model model, HttpServletRequest uriBuilder,Pageable pageable) {

        var uri = uriBuilder.getRequestURI();
        var posts = ps.allPosts(pageable);

        constructPageable(posts,propertiesService.getDefaultPageSize(),model,uri);
        return "main";
    }

    @GetMapping("/registration")
    public String pageRegisterCustomer(Model model) {
        if (!model.containsAttribute("dto")) {
            model.addAttribute("dto", new CustomerRegisterForm());
        }
        return "registration";
    }

    @PostMapping("/registration")
    public String registerPage(@Valid CustomerRegisterForm customerRequestDto, BindingResult validationResult, RedirectAttributes attributes) {
        attributes.addFlashAttribute("dto", customerRequestDto);

        if (validationResult.hasFieldErrors()) {
            attributes.addFlashAttribute("errors", validationResult.getFieldErrors());
            return "redirect:/main/registration";
        }

        cs.register(customerRequestDto);
        return "redirect:/main/login";
    }

    @GetMapping("/login")
    public String loginPage(@RequestParam(required = false, defaultValue = "false") Boolean error, Model model) {
        model.addAttribute("error", error);
        return "login";
    }

    private static <T> void constructPageable(Page<T> list, int pageSize, Model model, String uri) {
        if (list.hasNext()) {
            model.addAttribute("nextPageLink", constructPageUri(uri, list.nextPageable().getPageNumber(), list.nextPageable().getPageSize()));
        }

        if (list.hasPrevious()) {
            model.addAttribute("prevPageLink", constructPageUri(uri, list.previousPageable().getPageNumber(), list.previousPageable().getPageSize()));
        }

        model.addAttribute("hasNext", list.hasNext());
        model.addAttribute("hasPrev", list.hasPrevious());
        model.addAttribute("items", list.getContent());
        model.addAttribute("defaultPageSize", pageSize);
    }
    private static String constructPageUri(String uri, int page, int size) {
        return String.format("%s?page=%s&size=%s", uri, page, size);
    }
    @GetMapping("/logout")
    public String invalidate(HttpSession session) {
        if (session != null) {
            session.invalidate();
        }

        return "redirect:/main";
    }

    @GetMapping("/newTheme")
    public String newTheme(){

        return "newTheme";
    }
    @PostMapping("/newTheme")
    public String newPost(@RequestParam("text")String text,Authentication authentication, @RequestParam("name")String name){
        ps.newTheme(authentication,text,name);
        return "redirect:/main";
    }

    @GetMapping("/posts/{id}")
    public String getPost(@PathVariable("id") int id,Authentication authentication,Model model,Pageable pageable,HttpServletRequest uriBuilder){

        var post = ps.getPost(id,authentication);
        var uri = uriBuilder.getRequestURI();
        var comments = commentService.allComments(id,pageable);

        constructPageable(comments,propertiesService.getDefaultPageSize(),model,uri);
        model.addAttribute("post", post);

        return "only_theme";
    }

    @GetMapping("/profile")
    public String getProfile(Authentication authentication,Model model){

        var user = cs.getLoginCustomer(authentication);
        model.addAttribute("customer",user);

        return "profile";
    }
    @PostMapping("/newComment")
    public String newComment(@RequestParam("text") String text, @RequestParam("id")int id, Authentication authentication){
        commentService.newComment(text, authentication, id);
        return "redirect:/main/posts/"+id;
    }
}
