//package kz.attractor.exam9.Controller;
//
//import kz.attractor.exam9.model.Comment;
//import kz.attractor.exam9.model.Theme;
//import kz.attractor.exam9.service.CommentService;
//import kz.attractor.exam9.service.CustomerService;
//import kz.attractor.exam9.service.ThemeService;
//import lombok.AllArgsConstructor;
//import org.springframework.security.core.context.SecurityContextHolder;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//
//@Controller
//@AllArgsConstructor
//public class ThemeController {
//    private final ThemeService ts;
//    private final CustomerService cs;
//    private final CommentService commentService;
//
////    public ThemeController(ThemeService ts, CustomerService cs, CommentService commentService) {
////        this.ts = ts;
////        this.cs = cs;
////        this.commentService = commentService;
////    }
//
//    @PostMapping("/theme/add/comment")
//    public void addComment(@RequestParam("theme_id") Integer theme_id, @RequestParam("text") String text){
//        Comment comment = new Comment();
//        comment.setCustomer(cs.findByFullname(getUsername()));
//        comment.setText(text);
//        comment.setTheme(ts.findThemeById(theme_id));
//        commentService.saveComment(comment);
//    }
//
//    @PostMapping("/create/theme")
//    public String createTheme(@RequestParam("title") String title, @RequestParam("main_text") String text){
//        Theme theme = new Theme();
//        theme.setTitle(title);
//        theme.setText(text);
//        theme.setCustomer(cs.findByFullname(getUsername()));
//        ts.saveTheme(theme);
//        return "redirect:/";
//    }
//
//    private String getUsername(){
//        var authentication = SecurityContextHolder.getContext().getAuthentication();
//        return authentication.getName();
//    }
//}