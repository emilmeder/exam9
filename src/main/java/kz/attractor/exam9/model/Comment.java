package kz.attractor.exam9.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.sql.Timestamp;


@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor
@Entity
@ToString
@Table(name = "comments")
public class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @NotBlank
    @Size(min = 1, max = 128)
    @Column(length = 128)
    private String text;
    @OneToOne(targetEntity = Post.class, fetch = FetchType.EAGER)
    @JoinColumn( name = "posts_id")
    private Post post;
    @Column()
    private Timestamp date;
    @ManyToOne(targetEntity = Customer.class, fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "customers_id")
    private Customer customer;
}
