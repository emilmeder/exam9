package kz.attractor.exam9.service;


import kz.attractor.exam9.dto.CommentDTO;
import kz.attractor.exam9.model.Comment;
import kz.attractor.exam9.repository.CommentRepository;
import kz.attractor.exam9.repository.CustomerRepository;
import kz.attractor.exam9.repository.PostRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;

@Service
@AllArgsConstructor
public class CommentService {
    private final CommentRepository cr;
    private final CustomerRepository customerRepository;
    private final PostRepository pr;

//    public void createComment(String text, Authentication authentication, int id){
//        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
//        var user = customerRepository.findByEmail(userDetails.getUsername());
//        var comment = Comment.builder()
//                .customer(user)
//                .date(new Timestamp(System.currentTimeMillis()))
//                .post(themeRepository.findById(id).get())
//                .text(text)
//                .build();
//        commentRepository.save(comment);
//
//    }

    public Page<CommentDTO> allComments(int id, Pageable pageable){
        return cr.findAllByPostId(id,pageable).map(CommentDTO::from);
    }

    public void newComment(String text, Authentication authentication,int id){
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();

        var user = customerRepository.findByEmail(userDetails.getUsername());
        var comment = Comment.builder().customer(user).date(new Timestamp(System.currentTimeMillis())).post(pr.findById(id).get()).text(text).build();
        cr.save(comment);

    }
}
