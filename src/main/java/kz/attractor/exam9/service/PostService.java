package kz.attractor.exam9.service;

import kz.attractor.exam9.dto.PostDTO;
import kz.attractor.exam9.model.Post;
import kz.attractor.exam9.repository.CustomerRepository;
import kz.attractor.exam9.repository.PostRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;

@Service
@AllArgsConstructor
public class PostService {
    private final PostRepository pr;
    private final CustomerRepository cr;

    public void newTheme(Authentication authentication, String text,String name){
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();

        var user = cr.findByEmail(userDetails.getUsername());
        var post = Post.builder().text(text).customer(user).date(new Timestamp(System.currentTimeMillis())).name(name).build();

        pr.save(post);
    }

    public Page<PostDTO> allPosts(Pageable pageable){

        var posts = pr.findAllByOrderByDateAsc(pageable);
        return posts.map(PostDTO::from);
    }
    public PostDTO getPost(int id, Authentication authentication){
        return PostDTO.from(pr.findById(id).get());
    }


}
