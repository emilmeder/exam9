package kz.attractor.exam9.repository;


import kz.attractor.exam9.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;


public interface CustomerRepository extends JpaRepository<Customer, Integer> {

    boolean existsByEmail(String email);

    //    boolean existsByEmail();
//
////    Optional<Customer> findByEmail(String email);
//

    //

    Customer findByEmail(String email);

}
