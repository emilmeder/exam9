package kz.attractor.exam9.repository;


import kz.attractor.exam9.model.Comment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommentRepository extends JpaRepository<Comment, Integer> {

//    //    Page<Comment> findAllById(Integer id);
//    Page<Comment> findAllByPostId(int id, Pageable pageable);
    //    Page<Comment> findAllByThemeId(Integer themeId, Pageable pageable);
    Page<Comment> findAllByPostId(int id, Pageable pageable);
}
