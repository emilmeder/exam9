package kz.attractor.exam9.repository;


import kz.attractor.exam9.model.Post;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface PostRepository extends JpaRepository<Post, Integer> {


    //    Page<Theme> findProductsByNameAndText(String name, Pageable pageable);
    Page<Post> findAllByOrderByDateAsc (Pageable pageable);

}