package kz.attractor.exam9.dto;


import kz.attractor.exam9.model.Comment;
import lombok.*;

import java.sql.Timestamp;

@Data
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Builder(access = AccessLevel.PACKAGE)
@ToString
public class CommentDTO {
    private int id;
    private String text;
    private Timestamp date;
    private CustomerDTO customer;
    private PostDTO post;

    public static CommentDTO from(Comment comment){
        return   builder()
                .customer(CustomerDTO.from(comment.getCustomer()))
                .date(comment.getDate())
                .id(comment.getId())
                .post(PostDTO.from(comment.getPost()))
                .text(comment.getText())
                .build();
    }
}
